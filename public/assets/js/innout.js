(function () {
    const menuToggle = document.querySelector('.toggle');
    menuToggle.onclick = function (e) {
        const body = document.querySelector('body');
        body.classList.toggle('hide-sidebar');
    }

})()
