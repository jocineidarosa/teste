<?php
function requireValidSession()
{
    if (isset($_SESSION['user'])) {
        $user = $_SESSION['user'];
        if (!isset($user)) {
            header('Location: login.php');
            exit();
        }
    } else {
        header('Location: login.php');
    }
}
